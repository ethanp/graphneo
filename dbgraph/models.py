# Just NOTE that in order to import into the Python interpreter, you need
#   to export DJANGO_SETTINGS_MODULE=graphneo.settings first.
#  Otw the interpreter has no access to your settings.

from neo4django.db import models
import neo4django


###################################
##
# The FAERS DB
##
###################################

# Sample Drugs
#  Tylenol
#  Sudafed
#  Advil

# Through CaseID#34456:
#  Tylenol & Sudafed produce:
#       Wheezing to Death
#       Diarrhea

class Cases(models.NodeModel):
    caseId = models.StringProperty()


class Drug(models.NodeModel):
    name = models.StringProperty()


class SideEffect(models.NodeModel):
    title = models.StringProperty()


#class DrugProminence(models.NodeModel):
    #drugName = models.Relationship(Drug,
                                    #rel_type=neo4django.Incoming.OWNS,
                                    #single=True,
                                    #related_name=
            



#***********************************************************************

###################################
##
# The Given Sample DB
##
###################################


class Person(models.NodeModel):
    name = models.StringProperty()
    age = models.IntegerProperty()

class OnlinePerson(Person):
    email = models.EmailProperty()
    homepage = models.URLProperty()

class EmployedPerson(Person):
    job_title = models.StringProperty(indexed=True)

class Pet(models.NodeModel):
    owner = models.Relationship(Person,
                                rel_type=neo4django.Incoming.OWNS,
                                single=True,
                                related_name='pets'
                               )


